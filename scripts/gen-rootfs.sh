#!/bin/bash
# Generate a rootfs image from Debian Jessie
# See https://www.collabora.com/news-and-blog/blog/2017/01/16/setting-up-qemu-kvm-for-kernel-development/
# -----------------------------------------

#--------------------------------------------------------------
# Initialization
#--------------------------------------------------------------
IMG=qemu-image.img
DIR=mnt
SIZE=1
DOFIXUP=0

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------
# Provide command line usage assistance
function doHelp
{
    echo ""
    echo "$0 [-f | -i image | -m mntpt | -s size ]"
    echo "where"
    echo "-f            Just update the root password."
    echo "-i image      Image filename"
    echo "              Default: $IMG"
    echo "-m mntpt      Where to mount the image."
    echo "              Default: $DIR"
    echo "-s size       Specify the size of the image, in GB."
    echo "              Default: $SIZE"
}

function fixup
{
	# Add these to /etc/fstab
    # Default jessie via debootstrap doesn't set them.
    grep vda1 $DIR/etc/fstab 2>&1 >/dev/null
    if [ $? -eq 1 ]
    then
        cp $DIR/etc/fstab $DIR/tmp/
	    echo "proc            /proc           proc    defaults          0 0" >> $DIR/tmp/fstab
	    echo "/dev/vda1       /               ext2    errors=remount-ro 0 1" >> $DIR/tmp/fstab
	    echo "/dev/hdc        /media/cdrom0   udf,iso9660 user,noauto   0 0" >> $DIR/tmp/fstab
        sudo mv $DIR/tmp/fstab $DIR/etc/fstab
    fi

	# Change root passwd
    if [ "$ROOTPW" != "" ]
    then
        echo "Changing root password."
        echo sudo chroot $DIR sh -c "echo root:$ROOTPW | chpasswd"
        sudo chroot $DIR sh -c "echo root:$ROOTPW | chpasswd"
    fi
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":fi:m:s:" Option
do
    case $Option in
    i) IMG=$OPTARG;;
    m) MNT=$OPTARG;;
    s) SIZE=$OPTARG;;
    f) DOFIXUP=1;;
    *) doHelp; exit 0;;
    esac
done

# Source your local config.  This way you can set your own user/password.
if [ ! ~/.kdevrc ]
then
    echo "Can't find ~/.kdevrc.  Won't change root password."
else
    . ~/.kdevrc
fi

# if requested, just run the fixup bit.
if [ $DOFIXUP -eq 1 ]
then
    if [ ! -f $IMG ]
    then
        echo "Missing image file."
        exit 1
    fi
    mkdir -p $DIR
    sudo mount -o loop $IMG $DIR
    fixup
    sudo umount $DIR
    rmdir $DIR
    exit 0
fi

#--------------------------------------------------------------
# Main
#--------------------------------------------------------------
qemu-img create $IMG ${SIZE}g
mkfs.ext2 $IMG
mkdir -p $DIR
sudo mount -o loop $IMG $DIR
sudo debootstrap --arch amd64 jessie $DIR
fixup
sudo umount $DIR
rmdir $DIR
