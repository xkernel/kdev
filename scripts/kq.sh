#!/bin/bash
# Start the qemu session with a custom kernel
# -------------------------------------------

#--------------------------------------------------------------
# Initialization
#--------------------------------------------------------------
# Default qemu executable
QEMU=qemu-system-x86_64

# Default kernel is the one on your system.
# Probably never want to use this one.
# KERNEL=/boot/vmlinuz-`uname -r`
KERNEL=../../pkg/vmlinuz
# INITRD=/boot/initramfs-`uname -r`.img

# Default rootfs image
ROOTFS=qemu-image.img

# If set, only test booting the kernel.
DOTEST=0

# If set, boots into single user mode.
SINGLE=

# If set, enable graphic mode (separate X window for QEMU session)
GRAPHIC=0
NOGRAPHIC="--nographic"
CONSOLE="console=ttyS0"

# Sets up networking to QEMU host
NET="-net nic -net user"

# If set, boot qemu with -s option to make gdb debugging possible.
GDB=

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------
# Provide command line usage assistance
function doHelp
{
    echo ""
    echo "$0 [-gGnst | -k kernel | -r rootfs | -q qemu]"
    echo "where"
    echo "-g            Use separate X window for QEMU session"
    echo "-t            Just test kernel booting, without rootfs"
    echo "-s            Boot into single user mode"
    echo "-G            Boot with GDB enabled"
    echo "-n            Disable networking in QEMU"
    echo "-k kernel     Kernel to use for booting."
    echo "              Default: $KERNEL"
    echo "-r rootfs     Root file system to use for booting."
    echo "              Default: $ROOTFS"
    echo "-q qemu       QEMU executable to use."
    echo "              Default: $QEMU"
}

# Test: no rootfs, so should fail
function testNoRootFS {
    $QEMU -kernel $KERNEL
}


#--------------------------------------------------------------
# Source local config.  Command line args will override.
#--------------------------------------------------------------
if [ -f ~/.kdevrc ]
then
    . ~/.kdevrc
fi

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":gGnstk:q:r:" Option
do
    case $Option in
    g) GRAPHIC=1;;
    G) GDB="-s";;
    k) KERNEL=$OPTARG;;
    n) NET="-net nic -net user,hostfwd=tcp::5555-:22";;
    q) QEMU=$OPTARG;;
    r) ROOTFS=$OPTARG;;
    s) SINGLE=single;;
    t) DOTEST=1;;
    *) doHelp; exit 0;;
    esac
done

#--------------------------------------------------------------
# Pre-run:  setup variables dependent on initialization
# variables.
#--------------------------------------------------------------
# Different ways to specify hard drive
# This one will generate a warning about a raw format
HDA="-hda $ROOTFS"
# This one will remove the warning about a raw format
# HDA="-drive file=$ROOTFS,index=0,media=disk,format=raw"

if [ $GRAPHIC -eq 1 ]
then
    NOGRAPHIC=
    CONSOLE=
fi

#--------------------------------------------------------------
# Main
#--------------------------------------------------------------

# If requested, just test booting the kernel without a rootfs.
if [ $DOTEST -eq 1 ]
then
    testNoRootFS
    exit 0
fi

# Run QEMU with specified configuration
$QEMU -kernel $KERNEL \
              $HDA \
              -append "root=/dev/sda $SINGLE $CONSOLE" \
              $NOGRAPHIC \
              $GDB \
              $NET \
              --enable-kvm 
