#----------------------------------------------------------------------
# Common Config variables and targets.
# Includes configurations from configs directory.
#----------------------------------------------------------------------

#---------------------------------------------------------------------
# Edit these for a new build environment

# Project Architecture and Vendor
ifeq ($(XA),)
ARCH = x86
else
ARCH = $(XA)
endif

# Hardware
# Specified by the *Box project, all lowercase.
HW = pibox

# Build Version ID
BLD_VERSION := $(shell cat version.txt)

#---------------------------------------------------------------------
# End of Build Configurable options
#---------------------------------------------------------------------
# Number of parallel jobs.  Override this on the command line.
JOBS = 4

# Who am I?
UID = $(shell id -u)

#---------------------------------------------------------------------
# Target names - these are also used with a "." prefix as
# empty target files for various build sections.
# TARGETS gets updated in each component's .cfg file.

INIT_T          := init
TARGETS         = 

#---------------------------------------------------------------------
# Directories: 
# The build and archive downloads are kept in parallel directories from
# the source tree so hg status won't get confused by all the new files.
TOPDIR              := $(shell pwd)
SRCDIR              := $(TOPDIR)/src
ARCDIR              := $(TOPDIR)/../archive
BLDDIR              := $(TOPDIR)/../bld
PKGDIR              := $(TOPDIR)/../pkg
SCRIPTDIR           := $(TOPDIR)/scripts

#---------------------------------------------------------------------
# These variables can be set as environment variables on the "make" command line

# The location of the cross toolchain is set with XI
ifeq ($(XI),)
XCC_PREFIXDIR = 
else
XCC_PREFIXDIR = $(XI)
endif

# Don't use a cross compiler if the toolchain directory is not set.
ifeq ($(XCC_PREFIXDIR),)
CROSS_COMPILER := 
else
CROSS_COMPILER := CROSS_COMPILE=$(XCC_PREFIX)-
endif

# Where we can find the opkg-build utility is set with OPKG
ifeq ($(OPKG),)
OPKG_DIR = /usr/local/bin
else
OPKG_DIR = $(OPKG)
endif

# Where is the rootfs?
ROOTFS = $(SCRIPTDIR)/qemu-image.img

# Where do we mount the rootfs when making changes to it from the build?
MNT = $(SCRIPTDIR)/mnt

#---------------------------------------------------------------------
# Include the configs directory files after the common configs
# Note: Order here is important

include configs/kernel.cfg

#---------------------------------------------------------------------
# Include the component makefiles
# Note: Order here is important
include configs/kernel.mk

#---------------------------------------------------------------------
# Config display target
showconfig:
	@$(MSG3) Common Configuration $(EMSG)
	@echo "Components           :$(TARGETS)"
	@echo "Version              : $(BLD_VERSION)"
	@echo "ARCH                 : $(ARCH)"
	@echo "HW                   : $(HW)"
	@echo "SRCDIR               : $(SRCDIR)"
	@echo "ARCDIR               : $(ARCDIR)"
	@echo "BLDDIR               : $(BLDDIR)"
	@echo "XCC_PREFIXDIR        : $(XCC_PREFIXDIR)"
	@echo "XCC_PREFIX           : $(XCC_PREFIX)"
	@echo "CROSS_COMPILER       : $(CROSS_COMPILER)"
	@echo "OPKG_DIR             : $(OPKG_DIR)"

